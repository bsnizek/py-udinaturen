# -*- coding: utf-8 -*-
import requests

__author__ = 'besn'
from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)
import json
import urllib

__author__ = 'Bernhard Snizek <b@informal.dk>'

class UdiNaturenLanguage(object):

    def __init__(self, d={}):
        self.code = d.get("Code")
        self.name = d.get("Name")
        self.language_id = d.get("LanguageID")

    @property
    def id(self):
        """
        Convenience property.
        :return:
        """
        return self.language_id

    def __repr__(self):
        return "<UdiNaturenLanguage %d:%s>" % (self.id, self.name.encode('utf8'))


class UdiNaturenOrganisation(object):

    def __init__(self, d):
        self.organisation_id = d.get("OrganisationID")
        self.organisation_name = d.get("OrganisationName")
        self.telephone = d.get("Telephone")
        self.url = d.get("Url")
        self.logo_image_id = d.get("LogoImageID")
        self.email = d.get("Email")

    def __repr__(self):
        return "<UdiNaturenOrganisation : %s>" % self.organisation_name.encode('utf8')

    @property
    def id(self):
        return self.organisation_id

    @property
    def name(self):
        return self.organisation_name


class UdiNaturenFacilityData(object):

    def __init__(self, d= {}):
        # TODO: add much  more attributes here
        self._raw_dict = d
        self.rejseplan_link_tilladt = d.get("RejseplanLinkTilladt")
        self.facility_id = d.get("FacilityID")
        self.name = d.get("Name")
        self.facility_geometry_wkt = d.get("FacilityGeometryWKT")
        self.url = d.get("url")
        self.geometry_type = d.get("GeometryType")
        self.long_description = d.get("LongDescription")
        self.short_description = d.get("ShortDescription")
        self.subcategory_id = d.get("SubCategoryID")
        self.trip = d.get("Trip")
        self.url = d.get("Url")
        self.main_image_id = d.get("MainImageID")
        self.is_publicly_available = d.get("IsPublicAvailable")

        self.organisation = UdiNaturenOrganisation(d.get("Organisation"))
        self.subcategory = UdiNaturenSubCategory(d.get("SubCategory"))

    def __repr__(self):
        try:
            return "<UdiNaturenFacilityData [%s]: %s>" % (self.subcategory.name, self.name.encode('utf8'))
        except:
            return "<UdiNaturenFacilityData>"



class UdiNaturenResultItem(object):

    def __init__(self, dict):

        # pprint(dict)
        self.facility_id = dict.get('FacilityID')
        self.long_description = dict.get('LongDescription')
        self.name = dict.get("Name")
        self.short_description = dict.get("ShortDescription")

    def __repr__(self):
        import pdb;pdb.set_trace()
        return "<UdiNaturenResultItem: %d %s>" % (self.facility_id, "x")

class UdiNaturenCategory(object):

    def __init__(self, d={}):
        self.name = d.get("Name")
        self.sort = d.get("Sort")
        self.subcategory_list = d.get('SubCategoryList')

    def get_subcategories(self):
        """Returns a list of UdiNaturenSubCategories.
        """
        return [UdiNaturenSubCategory(sc) for sc in self.subcategory_list]

    def get_subcategory(self, subcategory_id):
        return self.get_subcategories.get(subcategory_id)


class UdiNaturenSubCategory(object):

    def __init__(self, d={}):
        self.subcategory_id = d.get("SubCategoryID")
        self.sort = d.get("Sort")
        self.category_id = d.get("CategoryID")
        self.geometry_type = d.get("GeometryType")
        self.attribute_list  = d.get("AttributeList")
        self.icon1image_id = d.get("Icon1ImageID")
        self.icon2image_id = d.get("Icon1ImageID")
        self.icon3image_id = d.get("Icon1ImageID")
        self.icon4image_id = d.get("Icon1ImageID")
        self.is_trip = d.get("IsTrip")
        self.name = d.get("Name")
        self.web_booking = d.get("WebBooking")

    @property
    def id(self):
        return self.subcategory_id

class UdiNaturenAttribute(object):

    def __init__(self, d={}):
        self.name = d.get("Name")
        self.attribute_id = d.get("AttributeID")

    @property
    def id(self):
        """Convenience property.
        """
        return self.attribute_id

    def __repr__(self):
        return ("<UdiNaturenAttribute %d: %s>" % (self.id, self.name)).encode("utf8")


class UdiNaturenAPIException(object):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)



class UdiNaturenAPI(object):
    """
    API
    """
    RESULT_FORMAT = 'json'  # or xml

    BASE_URL = 'http://udinaturen.naturstyrelsen.dk/wcf/Service.svc/%s/' % RESULT_FORMAT

    def __init__(self):
        pass

    def call(self, endpoint, **kwargs):

        arguments = urllib.urlencode(kwargs)

        url = "%s%s?%s" % (self.BASE_URL, endpoint, arguments)

        result = urllib.request.urlopen(url)

        return json.load(result)

    def call_by_url(self, url):
        header = {'x-requested-with': 'XMLHttpRequest'}
        return requests.get(url, headers = header).json()

    def get_language_list(self):
        """
        :param kwargs:
        :return:
        """
        _endpoint = 'GetLanguageList'

        get_language_list_result = self.call(_endpoint)
        if get_language_list_result:
            language_list = get_language_list_result.get("LanguageList")
            if language_list:
                return [UdiNaturenLanguage(lng) for lng in language_list]

        return self.call(_endpoint)

    def get_language_by_code(self, language_code='da'):
        """
        Returns a UdiNaturenLanguage object for the given code
        :param code:
        :return:
        """
        language_list = self.get_language_list()
        language_dict = {}
        for lng in language_list:
            language_dict[lng.code] = lng
        return language_dict.get(language_code)



    def get_attribute_set(self, language_id=1):
        _url = "%sGetAttributeSet/%s" % (self.BASE_URL, str(language_id))
        result_dict = self.call_by_url(_url)
        if result_dict:
            attributes = result_dict.get("AttributeSet")
            if attributes:
                return [UdiNaturenAttribute(a) for a in attributes]
        raise UdiNaturenAPIException

    def get_attribute_set_as_id_list(self, language_id=1):
        results = self.get_attribute_set(language_id=language_id)
        if results:
            return [r.id for r in results]

    def get_category_list(self, language_id=1, include_subcategories=True):

        if include_subcategories is True:
            sc = 'true'
        else:
            sc = 'false'

        _url = "%sGetCategoryList/%s/%s" % (self.BASE_URL, language_id, sc)
        return self.call_by_url(_url)

    def find_facilities(self, language_id=1, attribute_id_list=None, subcategory_id_list='38', route_min_length=0, route_max_length=0):
        """
        TODO: add parameters
        :param language_id:
        :param attribute_id_list:
        :param subcategory_id_list:
        :return:
        """

        if attribute_id_list:

            _url = "%sFindFacilities/%s/%s/0/0?bbox=166021.4431,0.0000,833978.5569,9329005.1825?attributeIdList=%s" \
                   % (self.BASE_URL, language_id, subcategory_id_list, attribute_id_list)

        else:
            _url = "%sFindFacilities/%s/%s/0/0?bbox=166021.4431,0.0000,833978.5569,9329005.1825" \
                   % (self.BASE_URL, language_id, subcategory_id_list)

        return self.call_by_url(_url)

    def get_attribute_ids(self, subcategory_id):
        """
        Returns attribute ids for a subcategory as an array of ints.
        :param subcategory_id:
        :return:
        """

    def get_search_result_items(self, search_result_id, start_index=0, count=20, order_by=0, direction=1):

        _url = "%sGetSearchResultItems/%s/%d/%d/%d/%d" % (self.BASE_URL, search_result_id, start_index, count, order_by, direction)
        return self.call_by_url(_url)

    def get_facility_data(self, facility_id, language_id='1', epsg='1'):
        """http://udinaturen.naturstyrelsen.dk/wcf/Service.svc/xml/GetFacilityData/1/7701/1"""
        _url = "%sGetFacilityData/%s/%d/%s" % (self.BASE_URL, language_id, facility_id, epsg)
        _data = self.call_by_url(_url)
        return UdiNaturenFacilityData(_data)

    def get_subcategory(self, subcategory_id):
        # TODO
        pass


def print_category_list(language='1'):

    api = UdiNaturenAPI()

    category_list = (api.get_category_list(1))

    for cat in category_list.get("CategoryList"):
        print("CAT: %s : %s" % (cat.get("CategoryID"), cat.get("Name")))
        for subcategory in cat.get("SubCategoryList"):
            print("SUBCAT: %s : %s" % (subcategory.get("SubCategoryID"), subcategory.get("Name")))
        print("=======================================================================================")