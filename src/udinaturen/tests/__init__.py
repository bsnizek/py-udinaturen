# -*- coding: utf-8 -*-
__author__ = 'besn'
from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)


from udinaturen.api import UdiNaturenAPI


def import_test():

    api = UdiNaturenAPI()

    # Thats what we want
    # CAT 9 - overnatning
    cat_9_subcategories = [38, 39, 40]
    cat_8_subcategories = [13, 16, 15, 33, 34, 37]

    subcategories = cat_9_subcategories + cat_8_subcategories

    s = ''
    for c in subcategories:
        s = "%s,%d" % (s, c)

    subcategories_string = s[1:]

    facility_search_results = api.find_facilities(subcategory_id_list=subcategories_string)

    print("%d results found" % facility_search_results.get("Count"))

    yes = True
    start_index = 0
    count = 20
    fc = 0

    search_id = facility_search_results.get('SearchResultID')

    while yes:

        results = api.get_search_result_items(search_id, start_index, 20).get("SearchResultItemList")

        result_items = [UdiNaturenResultItem(result) for result in results]

        for result in result_items:
            facility_data = api.get_facility_data(result.facility_id)
            if facility_data.geometry_type == u'POINT':
                fc += 1
                print("%d : %s" % (fc, facility_data))

        if results is not None:
            start_index += count
        else:
            yes = False

    print("Final result. %d POINTs " % fc)