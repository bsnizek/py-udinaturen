#!/usr/bin/env python

from setuptools import setup, find_packages

version = '0.0.1'

setup(
        name='py-udinaturen',
        version='0.0.1',
        description='A module to access the Ud i naturen API',
        author='Bernhard Snizek',
        author_email='b@snizek.com',
        maintainer='Bernhard Snizek',
        maintainer_email='b@snizek.com',
        url='http://bitbucket.org/bsnizek/py-udinaturen',
        license='Apache License 2.0',
        classifiers=[
            'Development Status :: 4 - Beta',
            'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
            'Programming Language :: Python',
            'Programming Language :: Python :: 3.4'
        ],
        package_dir={'': 'src'},
        packages=find_packages('src'),
        # namespace_packages=['hagr'],
        include_package_data=True,
        platforms="Any",
)